// Copyright 2020 Dan Kestranek.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "GSMenu.generated.h"

/**
 * 
 */
UCLASS()
class GASSHOOTER_API UGSMenu : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(meta = (BindWidget))
	UButton* NewSessionButton;

	UPROPERTY(meta = (BindWidget))
	UButton* JoinSessionButton;
	
};
