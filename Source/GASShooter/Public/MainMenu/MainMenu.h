// Copyright 2020 Dan Kestranek.

#pragma once

#include "SessionMenuInterface.h"
#include "SessionRow.h"

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MainMenu.generated.h"

USTRUCT()
struct FServerData
{
	GENERATED_BODY()

	FString Name;
	uint16 CurrentPlayers;
	uint16 MaxPlayers;
	FString HostUserName;
};

/**
 * 
 */
UCLASS()
class GASSHOOTER_API UMainMenu : public UUserWidget
{
	GENERATED_BODY()
	

public:

	void Setup(ISessionMenuInterface* MenuInterface);
	void TearDown();


protected:

	virtual bool Initialize() override;

protected:

	ISessionMenuInterface* SessionMenuInterface;

protected:

	// Menu widgets
	UPROPERTY(meta = (BindWidget))
		class UWidgetSwitcher* MenuSwitcher;

	UPROPERTY(meta = (BindWidget))
		class UWidget* HostSessionMenuWidget;


	// Buttons
	UPROPERTY(meta = (BindWidget))
		class UButton* NewSessionButton;

	UFUNCTION()
		void OnNewSessionPressed();

	UPROPERTY(meta = (BindWidget))
		class UButton* JoinSessionButton;

	UFUNCTION()
		void OnJoinSessionPressed();


	// Join avaiable Sessions //

	UPROPERTY(meta = (BindWidget))
		class UWidget* SessionListMenuWidget;

	UPROPERTY(meta = (BindWidget))
		class UPanelWidget* ScrollSessionList;

	UPROPERTY(EditAnywhere)
		TSubclassOf<class USessionRow> SessionRowClass;
		TOptional<uint32> SelectedScrollIndex;

	// Buttons for the session list

	UPROPERTY(meta = (BindWidget))
		class UButton* CancelJoinSessionButton;

	UFUNCTION()
		void OnCancelJoinSession();

	UPROPERTY(meta = (BindWidget))
		class UButton* JoinSelectedSessionButton;

	UFUNCTION()
		void OnJoinSelectedSession();


public:

	void InitializeSessionsList(TArray<FServerData> Data);
	void SelectIndexSessionsList(uint32 Index);
	void UpdateSessionsList();
};
