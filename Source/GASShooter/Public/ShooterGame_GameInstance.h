// Copyright 2020 Dan Kestranek.

#pragma once

#include "MainMenu/SessionMenuInterface.h"

#include <Source/Public/OnlineSubsystem.h>
#include <Source/Public/Interfaces/OnlineSessionInterface.h>
#include "FindSessionsCallbackProxy.h"

#include "OnlineSessionSettings.h"
#include "CoreMinimal.h"
#include "Delegates/Delegate.h"
#include "Engine/GameInstance.h"
#include "UObject/CoreOnline.h"
#include "ShooterGame_GameInstance.generated.h"

USTRUCT(BlueprintType)
struct FSessionResult
{
	GENERATED_BODY()

	FOnlineSessionSearchResult SearchResult;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSessionsFoundDelegate, const TArray<FSessionResult>&, Sessions);

/**
 * 
 */
UCLASS()
class GASSHOOTER_API UShooterGame_GameInstance : public UGameInstance, public ISessionMenuInterface
{
	GENERATED_BODY()
	

public:
	
	UShooterGame_GameInstance(const FObjectInitializer& ObjectInitialiser);

	UPROPERTY(BlueprintAssignable)
	FOnSessionsFoundDelegate OnSessionsFound;

	UFUNCTION(BlueprintCallable)
	void LoadMainMenu();

	virtual void Init();

private:

	// Declaration of SessionInterface for the subsystem.
	IOnlineSessionPtr SessionInterface;
	// Shared pointer to session
	TSharedPtr<class FOnlineSessionSearch> SessionSearch;

	void OnParticipantsChanged(FName name, const FUniqueNetId& netId, bool joined);
	void OnRegisterPlayersComplete(FName sessionName, const TArray< TSharedRef<const FUniqueNetId> >& players, bool bWasSuccessful);
	void OnSessionFailure(const FUniqueNetId& playerId, ESessionFailure::Type failureType);

public:
	// TODO : ISessionMenu Interface
	UFUNCTION(BlueprintCallable, Category = "Shooter Game Instance | Session")
	void Host(FString ServerName) override;

	UFUNCTION()
	void JoinSession(uint32 Index) override;

	UFUNCTION(BlueprintCallable, Category = "Shooter Game Instance | Session")
	void DoJoinSession(int32 Index);

	UFUNCTION(BlueprintCallable, Category = "Shooter Game Instance | Session")
	void DoJoinSessionWithResult(FSessionResult sessionResult);

	UFUNCTION(BlueprintCallable, Category = "Shooter Game Instance | Session")
	void EndSession() override;

	UFUNCTION(BlueprintCallable, Category = "Shooter Game Instance | Session")
	void OpenSessionListMenu() override;

	UFUNCTION(BlueprintCallable, Category = "Shooter Game Instance | Session")
	void FindSessions();

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	FString HostURL = "";

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	FName LocalSessionName = "";

	// Main Menu declarations
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TSubclassOf<class UUserWidget> TheMenuClass;

	UFUNCTION(BlueprintCallable)
	void Travel(FString url);

	UFUNCTION(BlueprintCallable)
	void ClientTravel(FString url);

private:
	class UMainMenu* MainMenu;

private:

	bool bIsExistingSessionAvailable = false;

	// Callback function for create session complete
	void OnCreateSessionComplete(FName SessionName, bool bIsSuccess);
	// Callback function for destroy session complete
	void OnDestroySessionComplete(FName SessionName, bool bIsSuccess);
	// Callback function for finding sessions complete
	void OnFindSessionsComplete(bool bIsSuccess);
	// Callback function for joining sessions complete
	void OnJoiningSessionsComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);

	void CreateSession();
};
