// Copyright 2020 Dan Kestranek.

#pragma once

#include "CoreMinimal.h"
#include <BehaviorTree/BTTaskNode.h>
#include "BTTask_ShootNode.generated.h"

/**
 * 
 */
UCLASS()
class GASSHOOTER_API UBTTask_ShootNode : public UBTTaskNode
{
	GENERATED_BODY()
	
public:
	UBTTask_ShootNode();

protected:

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComponent, uint8* NodeMemory) override;

	virtual EBTNodeResult::Type AbortTask(UBehaviorTreeComponent& OwnerComponent, uint8* NodeMemory) override;
};
