// Copyright 2020 Dan Kestranek.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include <BehaviorTree/BlackboardComponent.h>
#include <BehaviorTree/BehaviorTreeTypes.h>
#include "SG_BTTask_BlackboardBase.generated.h"

/**
 * 
 */
UCLASS()
class GASSHOOTER_API USG_BTTask_BlackboardBase : public UBTTask_BlackboardBase
{
	GENERATED_BODY()


public:
	USG_BTTask_BlackboardBase();


protected:

		/* Override the EBT Node Result of this Blackboard for ExecuteTask method.
			returns EBTNodeResult::Succeeded or EBTNodeResult::Failed */
		virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComponent, uint8* NodeMemory) override;

		virtual EBTNodeResult::Type AbortTask(UBehaviorTreeComponent& OwnerComponent, uint8* NodeMemory) override;
};
