// Copyright 2020 Dan Kestranek.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Services/BTService_BlackboardBase.h"
#include "SG_BTService_BlackboardBase.generated.h"

/**
 * 
 */
UCLASS()
class GASSHOOTER_API USG_BTService_BlackboardBase : public UBTService_BlackboardBase
{
	GENERATED_BODY()
	
};
