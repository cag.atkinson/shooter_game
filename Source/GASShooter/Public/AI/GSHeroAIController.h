// Copyright 2020 Dan Kestranek.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include <BehaviorTree/BlackboardComponent.h>
#include "GSHeroAIController.generated.h"

/**
 * 
 */
UCLASS()
class GASSHOOTER_API AGSHeroAIController : public AAIController
{
	GENERATED_BODY()
	
public:
	AGSHeroAIController();
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
		float PatrolRadius = 0.0f;

	UPROPERTY(EditAnywhere)
		bool bCanShootAtTarget = false;

private:

	UPROPERTY(EditAnywhere)
		class UBlackBoardComponent* BlackBoardComponent;

	UPROPERTY(EditAnywhere)
		class UBehaviorTree* AIBehaviourTree;
	
	UPROPERTY(EditAnywhere)
		FVector AIHeroLocation;

	UPROPERTY(EditAnywhere)
		FVector AIHeroStartLocation;

protected:

	virtual void BeginPlay() override;
	virtual void OnPossess(APawn* inputPawn) override;
	virtual void PostInitializeComponents() override;

	void DoMovement(const FVector& location);
	void DoMovementToPlayer(AActor& player);
	void StopDoMovement();


	void SetupBlackboardVariables();

};
