// Copyright 2020 Dan Kestranek.


#include "AI/SG_BTTask_MoveToRandomLocation.h"
#include <NavigationSystem/Public/NavigationSystem.h>
#include "../../Public/AI/GSHeroAIController.h"

USG_BTTask_MoveToRandomLocation::USG_BTTask_MoveToRandomLocation()
{
	NodeName = TEXT("Move To Random Location");
}

EBTNodeResult::Type USG_BTTask_MoveToRandomLocation::ExecuteTask(UBehaviorTreeComponent& OwnerComponent, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComponent, NodeMemory);
	
	UNavigationSystemV1* NavSystem = Cast<UNavigationSystemV1>(GetWorld()->GetNavigationSystem());
	AGSHeroAIController* Owner = Cast<AGSHeroAIController>(OwnerComponent.GetAIOwner());
	FNavLocation RandomLocation;
	ANavigationData* NavData = FNavigationSystem::GetCurrent<UNavigationSystemV1>(GetWorld())->GetDefaultNavDataInstance();
	FSharedConstNavQueryFilter Filter;
	UBlackboardComponent* Blackboard = Owner->GetBlackboardComponent();

	float PatrolRadius = Blackboard->GetValueAsFloat(TEXT("PatrolRadius"));

	bool bIsSuccess = NavSystem->GetRandomPointInNavigableRadius(Owner->GetPawn()->GetActorLocation(), PatrolRadius, RandomLocation, NULL, Filter);
	if (bIsSuccess)
	{
		Owner->MoveToLocation(RandomLocation.Location);
		return EBTNodeResult::Succeeded;
	}
	else
	{
		return EBTNodeResult::Failed;
	}
}

EBTNodeResult::Type USG_BTTask_MoveToRandomLocation::AbortTask(UBehaviorTreeComponent& OwnerComponent, uint8* NodeMemory)
{
	Super::AbortTask(OwnerComponent, NodeMemory);

	return EBTNodeResult::Succeeded;
}
