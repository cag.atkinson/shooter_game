// Copyright 2020 Dan Kestranek.


#include "AI/BTTask_ShootNode.h"
#include <BehaviorTree/BlackboardComponent.h>
#include <BehaviorTree/BehaviorTree.h>
#include "../../Public/Characters/Heroes/GSHeroCharacter.h"
#include "../../Public/AI/GSHeroAIController.h"
#include "../../Public/Weapons/GSWeapon.h"
#include <Kismet/GameplayStatics.h>
#include "../../Public/Player/GSPlayerState.h"
#include "../../Public/Characters/Abilities/GSAbilitySystemComponent.h"

UBTTask_ShootNode::UBTTask_ShootNode()
{
	NodeName = TEXT("Shoot");
}

EBTNodeResult::Type UBTTask_ShootNode::ExecuteTask(UBehaviorTreeComponent& OwnerComponent, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComponent, NodeMemory);

	AGSHeroAIController* Owner = Cast<AGSHeroAIController>(OwnerComponent.GetAIOwner());
	AGSPlayerState* playerState = Owner->GetPlayerState<AGSPlayerState>();
	if (playerState)
	{
		UGSAbilitySystemComponent* abilities = Cast<UGSAbilitySystemComponent>(playerState->GetAbilitySystemComponent());
		if (abilities)
		{
			abilities->TryActivateAbilitiesByTag(FGameplayTagContainer(FGameplayTag::RequestGameplayTag("Ability.Weapon")));
		}
	}

	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange,
		FString::Printf(TEXT("[%S] Shoot bastards called!"), __func__));



	return EBTNodeResult::Succeeded;
}

EBTNodeResult::Type UBTTask_ShootNode::AbortTask(UBehaviorTreeComponent& OwnerComponent, uint8* NodeMemory)
{
	return EBTNodeResult::Type();
}
