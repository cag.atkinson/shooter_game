// Copyright 2020 Dan Kestranek.


#include "AI/GSHeroAIController.h"
#include <Kismet/GameplayStatics.h>
#include <GameFramework/Controller.h>

AGSHeroAIController::AGSHeroAIController()
{
	bWantsPlayerState = true;
}

void AGSHeroAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	UWorld* theWorld = GetWorld();
	if (theWorld != nullptr)
	{
		APawn* playerPawn = UGameplayStatics::GetPlayerPawn(theWorld, 0);

		if (LineOfSightTo(playerPawn))
		{
			GetBlackboardComponent()->SetValueAsVector(TEXT("PlayerLocation"), playerPawn->GetActorLocation());
			GetBlackboardComponent()->SetValueAsVector(TEXT("LastKnownPlayerLocation"), playerPawn->GetActorLocation());
			GetBlackboardComponent()->SetValueAsFloat(TEXT("DistanceToPlayer"), playerPawn->GetDistanceTo(GetPawn()));
			this->SetFocus(playerPawn);

		}
		else
		{
			// Clear player location
			GetBlackboardComponent()->ClearValue(TEXT("PlayerLocation"));
			this->ClearFocus(EAIFocusPriority::Gameplay);
		}
	}
}

void AGSHeroAIController::BeginPlay()
{
	Super::BeginPlay();

	if (AIBehaviourTree != nullptr)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, 
			FString::Printf(TEXT("[AGSHeroAIController::BeginPlay] RunBehaviorTree called!")));
		RunBehaviorTree(AIBehaviourTree);
		SetupBlackboardVariables();
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange,
			FString::Printf(TEXT("[AGSHeroAIController::BeginPlay] RunBehaviorTree not called!")));
	}
}

void AGSHeroAIController::OnPossess(APawn* inputPawn)
{
	Super::OnPossess(inputPawn);

	SetupBlackboardVariables();
}

void AGSHeroAIController::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	SetupBlackboardVariables();
}

void AGSHeroAIController::DoMovement(const FVector& location)
{
	this->MoveToLocation(location, 50.0f);
}

void AGSHeroAIController::DoMovementToPlayer(AActor& player)
{
	this->MoveToActor(&player);
}

void AGSHeroAIController::StopDoMovement()
{
	this->ClearFocus(EAIFocusPriority::Gameplay);
	this->StopMovement();
}

void AGSHeroAIController::SetupBlackboardVariables()
{
	if (GetPawn())
	{
		AIHeroStartLocation = GetPawn()->GetActorLocation();
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, FString::Printf(TEXT("[AGSHeroAIController::BeginPlay::GetPawn()] is nullptr! AIStartLocation is : %s"), *AIHeroStartLocation.ToString()));
	}

	if (GetBlackboardComponent())
	{
		GetBlackboardComponent()->SetValueAsVector(TEXT("StartLocation"), AIHeroStartLocation);
		GetBlackboardComponent()->SetValueAsFloat(TEXT("PatrolRadius"), PatrolRadius);
	}
}


