// Copyright 2020 Dan Kestranek.

#include "AI/SG_BTTask_BlackboardBase.h"

USG_BTTask_BlackboardBase::USG_BTTask_BlackboardBase()
{
	NodeName = TEXT("Clear Values");
}

EBTNodeResult::Type USG_BTTask_BlackboardBase::ExecuteTask(UBehaviorTreeComponent& OwnerComponent, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComponent, NodeMemory);

	OwnerComponent.GetBlackboardComponent()->ClearValue(GetSelectedBlackboardKey());

	return EBTNodeResult::Succeeded;
}

EBTNodeResult::Type USG_BTTask_BlackboardBase::AbortTask(UBehaviorTreeComponent& OwnerComponent, uint8* NodeMemory)
{
	Super::AbortTask(OwnerComponent, NodeMemory);

	return EBTNodeResult::Succeeded;
}
