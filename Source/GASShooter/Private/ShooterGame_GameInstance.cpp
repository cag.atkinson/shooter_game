// Copyright 2020 Dan Kestranek.


#include "ShooterGame_GameInstance.h"

#include "UObject/ConstructorHelpers.h"

#include <Source/Public/OnlineSubsystemTypes.h>

#include "MainMenu/MainMenu.h"


const static FName SESSION_NAME = TEXT("ShooterCO-OPGameSession");
const static FName SERVER_NAME_SETTINGS_KEY = TEXT("ServerName");

UShooterGame_GameInstance::UShooterGame_GameInstance(const FObjectInitializer& ObjectInitialiser)
{
	
}

void UShooterGame_GameInstance::LoadMainMenu()
{
	// TODO : Load main menu
	if (TheMenuClass == nullptr) return;

	MainMenu = CreateWidget<UMainMenu>(this, TheMenuClass);

	if (MainMenu == nullptr) return;

	MainMenu->Setup(this);
}

void UShooterGame_GameInstance::Init()
{
	// IOnlineSubsystem pointer populated by static get method
	IOnlineSubsystem* Subsystem = IOnlineSubsystem::Get();
	// From Subsystem get session interface.
	SessionInterface = Subsystem->GetSessionInterface();

	if (Subsystem != nullptr)
	{
		// Validity check against session populated by IOnlineSubsystem->GetSessionInterface
		if (SessionInterface.IsValid())
		{
			SessionInterface->OnCreateSessionCompleteDelegates.RemoveAll(this);
			SessionInterface->OnDestroySessionCompleteDelegates.RemoveAll(this);
			SessionInterface->OnFindSessionsCompleteDelegates.RemoveAll(this);
			SessionInterface->OnJoinSessionCompleteDelegates.RemoveAll(this);

			SessionInterface->OnCreateSessionCompleteDelegates.AddUObject(this, &UShooterGame_GameInstance::OnCreateSessionComplete);
			SessionInterface->OnDestroySessionCompleteDelegates.AddUObject(this, &UShooterGame_GameInstance::OnDestroySessionComplete);
			SessionInterface->OnFindSessionsCompleteDelegates.AddUObject(this, &UShooterGame_GameInstance::OnFindSessionsComplete);
			SessionInterface->OnJoinSessionCompleteDelegates.AddUObject(this, &UShooterGame_GameInstance::OnJoiningSessionsComplete);

			SessionInterface->OnSessionParticipantsChangeDelegates.AddUObject(this, &UShooterGame_GameInstance::OnParticipantsChanged);
			SessionInterface->OnRegisterPlayersCompleteDelegates.AddUObject(this, &UShooterGame_GameInstance::OnRegisterPlayersComplete);
			SessionInterface->OnSessionFailureDelegates.AddUObject(this, &UShooterGame_GameInstance::OnSessionFailure);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::Init] No Subsystem found!"));
	}
	
}

void UShooterGame_GameInstance::OnParticipantsChanged(FName name, const FUniqueNetId& netId, bool joined)
{
	UE_LOG(LogTemp, Warning, TEXT("On Participants Changed: [%s] [%s]"), *name.ToString(), (joined ? TEXT("true") : TEXT("false")));
}

void UShooterGame_GameInstance::OnRegisterPlayersComplete(FName sessionName, const TArray< TSharedRef<const FUniqueNetId> >& players, bool bWasSuccessful)
{
	UE_LOG(LogTemp, Warning, TEXT("OnRegisterPlayersComplete [%d] [%s]"), players.Num(), (bWasSuccessful ? TEXT("true") : TEXT("false")));
}

void UShooterGame_GameInstance::OnSessionFailure(const FUniqueNetId& playerId, ESessionFailure::Type failureType)
{
	UE_LOG(LogTemp, Warning, TEXT("On Session Failure:"));
}

void UShooterGame_GameInstance::Host(FString ServerName)
{
	LocalSessionName = FName(*ServerName);

	if (SessionInterface.IsValid())
	{
		FNamedOnlineSession* Existing = SessionInterface->GetNamedSession(LocalSessionName);

		if (Existing != nullptr)
		{
			UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::Host] An existing session is present and about to be destroyed!"));

			SessionInterface->DestroySession(LocalSessionName);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::Host] Creating new session!"));
			CreateSession();
		}
	}
}

void UShooterGame_GameInstance::JoinSession(uint32 Index)
{
	UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::JoinSession] Doing join session [%d]!"), Index);
	if (!SessionInterface.IsValid() || (!SessionSearch.IsValid())) return;
	UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::JoinSession] All valid!"));
	if (Index < (uint32)(SessionSearch->SearchResults.Num()))
	{
		const FOnlineSessionSearchResult& SearchResult = SessionSearch->SearchResults[Index];

		FString ServerName;
		if (!SearchResult.Session.SessionSettings.Get(SERVER_NAME_SETTINGS_KEY, ServerName))
		{
			ServerName = LocalSessionName.ToString();
		}

		UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::JoinSession] Attempting Join!"));
		SessionInterface->JoinSession(0, FName(*ServerName), SearchResult);
	}
}

void UShooterGame_GameInstance::DoJoinSession(int32 Index)
{
	/*FString serverName;
	if (!searchResult.OnlineResult.Session.SessionSettings.Get(SERVER_NAME_SETTINGS_KEY, serverName))
	{
		serverName = LocalSessionName.ToString();
	}

	UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::JoinSession] Attempting Join [%s]!"), *serverName);
	SessionInterface->JoinSession(0, FName(*serverName), searchResult.OnlineResult);*/
	JoinSession(Index);
}

void UShooterGame_GameInstance::DoJoinSessionWithResult(FSessionResult sessionResult)
{
	FString serverName;
	if (!sessionResult.SearchResult.Session.SessionSettings.Get(SERVER_NAME_SETTINGS_KEY, serverName))
	{
		serverName = LocalSessionName.ToString();
	}

	UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::JoinSession] Attempting Join [%s]!"), *serverName);
	SessionInterface->JoinSession(0, FName(*serverName), sessionResult.SearchResult);
}

void UShooterGame_GameInstance::EndSession()
{
	bool result = false;
	//?
	if (SessionInterface != nullptr)
	{
		if (this->LocalSessionName.IsValid())
		{
			result = SessionInterface->EndSession(this->LocalSessionName);
		}
	}

	if (result)
	{
		UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::EndSession] Successful ending of session => %s"), *this->LocalSessionName.ToString());
	}
}

void UShooterGame_GameInstance::OpenSessionListMenu()
{
	// Create session search pointer
	SessionSearch = MakeShareable(new FOnlineSessionSearch());
	if (SessionSearch != nullptr)
	{
		if (SessionSearch.IsValid())
		{
			SessionSearch->MaxSearchResults = 10;
			SessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);
			SessionInterface->FindSessions(0, SessionSearch.ToSharedRef());
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::OpenSessionListMenu] Session Search pointer is NULL, cannot find any sessions!"));
	}
}

void UShooterGame_GameInstance::FindSessions()
{
	// Create session search pointer
	SessionSearch = MakeShareable(new FOnlineSessionSearch());
	if (SessionSearch != nullptr)
	{
		if (SessionSearch.IsValid())
		{
			SessionSearch->MaxSearchResults = 10;
			SessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);
			SessionInterface->FindSessions(0, SessionSearch.ToSharedRef());
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::OpenSessionListMenu] Session Search pointer is NULL, cannot find any sessions!"));
	}
}

void UShooterGame_GameInstance::OnCreateSessionComplete(FName SessionName, bool bIsSuccess)
{
	if (bIsExistingSessionAvailable)
	{
		UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::OnCreateSessionComplete] OnCreateSessionComplete already Called!"));
		return;
	}

	if (!bIsSuccess)
	{
		UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::OnCreateSessionComplete] Session Completion failed!"));
		return;
	}

	UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::OnCreateSessionComplete] Successful completion of session => %s"), *SessionName.ToString());

	if (MainMenu != nullptr)
	{
		MainMenu->TearDown();
	}

	UWorld* theWorld = GetWorld();

	if (theWorld == nullptr) return;

	theWorld->ServerTravel(HostURL);
	bIsExistingSessionAvailable = true;
}

void UShooterGame_GameInstance::OnDestroySessionComplete(FName SessionName, bool bIsSuccess)
{
	if (bIsSuccess)
	{
		UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::OnDestroySessionComplete] Session Destruction Complete!"));
		CreateSession();
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::OnDestroySessionComplete] Session Destruction Unsuccessful!"));
	}
}

void UShooterGame_GameInstance::OnFindSessionsComplete(bool bIsSuccess)
{
	UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::OnFindSessionsComplete] Sessions Find Complete [%s]!"), bIsSuccess ? TEXT("true") : TEXT("false"));

	if (SessionSearch != nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::OnFindSessionsComplete] Sessions Search not nullptr!"));

		if (bIsSuccess && SessionSearch.IsValid())
		{
			UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::OnFindSessionsComplete] Sessions Search is valid!"));
			if (SessionSearch->SearchResults.Num() > 0)
			{
				UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::OnFindSessionsComplete] Sessions Find Complete [%d]!"), SessionSearch->SearchResults.Num());
				TArray<FServerData> ServerData;

				TArray<FSessionResult> SessionResults;

				for (const FOnlineSessionSearchResult& SearchResult : SessionSearch->SearchResults)
				{
					FServerData Data;
					FString ServerName;

					if (SearchResult.Session.SessionSettings.Get(SERVER_NAME_SETTINGS_KEY, ServerName))
					{
						Data.Name = ServerName;
					}

					Data.MaxPlayers = SearchResult.Session.SessionSettings.NumPublicConnections;
					Data.CurrentPlayers = Data.MaxPlayers - SearchResult.Session.NumOpenPublicConnections;
					Data.HostUserName = SearchResult.Session.OwningUserName;
					
					ServerData.Add(Data);

					FSessionResult sessionResult;
					sessionResult.SearchResult = SearchResult;

					SessionResults.Add(sessionResult);

					/*UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::OnFindSessionsComplete] Poking session 0!"));
					JoinSession(0);

					return;*/
				}

				//MainMenu->InitializeSessionsList(ServerData);

				OnSessionsFound.Broadcast(SessionResults);
			}
		}
	}
	
}

void UShooterGame_GameInstance::OnJoiningSessionsComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result)
{
	UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::OnJoiningSessionsComplete] Joining session complete called!"));

	if (MainMenu != nullptr)
	{
		MainMenu->TearDown();
	}

	if (!SessionInterface.IsValid()) return;

	if (!SessionInterface->GetResolvedConnectString(SessionName, HostURL))
	{
		UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::OnJoiningSessionsComplete] Couldn't get connection string!"));
		return;
	}

	UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::OnJoiningSessionsComplete] Uri : %s"), *HostURL);

	APlayerController* playerController = GetFirstLocalPlayerController();

	if (playerController == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::OnJoiningSessionsComplete] Player controller unable to find local player!"));
		return;
	}

	playerController->ClientTravel(HostURL, ETravelType::TRAVEL_Absolute);
}

void UShooterGame_GameInstance::CreateSession()
{
	if (bIsExistingSessionAvailable)
	{
		UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::CreateSession] A session already exists!"));
		return;
	}

	// Check we have a session interface
	if (SessionInterface != nullptr)
	{
		// Check session interface is valid
		if (SessionInterface.IsValid())
		{
			FOnlineSessionSettings Settings;
			Settings.NumPublicConnections = 3;
			Settings.bShouldAdvertise = true;
			Settings.bAllowJoinInProgress = true;
			Settings.bIsLANMatch = false;
			Settings.bUsesPresence = true;
			Settings.bAllowJoinViaPresence = true;

			Settings.Set(SERVER_NAME_SETTINGS_KEY, LocalSessionName.ToString());

			SessionInterface->CreateSession(0, LocalSessionName, Settings);

			/*// Create the container for our online settings.
			FOnlineSessionSettings SessionSettings;
			// Set internal data for our game.
			SessionSettings.bIsLANMatch = false;
			SessionSettings.NumPublicConnections = 3;
			SessionSettings.bShouldAdvertise = false;
			SessionSettings.bUsesPresence = true;

			// Allow invites for the session.
			SessionSettings.bAllowInvites = true;
			SessionSettings.bAllowJoinInProgress = true;
			SessionSettings.bIsDedicated = false;

			// Create the session with our defined settings
			SessionInterface->CreateSession(0, LocalSessionName, SessionSettings);*/
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::CreateSession] Session Interface pointer is NULL, cannot create the session!"));
	}
	
}

void UShooterGame_GameInstance::Travel(FString URL)
{
	UWorld* theWorld = GetWorld();

	if (theWorld == nullptr) return;

	theWorld->ServerTravel(URL);
}

void UShooterGame_GameInstance::ClientTravel(FString URL)
{
	APlayerController* playerController = GetFirstLocalPlayerController();

	if (playerController == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("[UShooterGame_GameInstance::OnJoiningSessionsComplete] Player controller unable to find local player!"));
		return;
	}

	playerController->ClientTravel(URL, ETravelType::TRAVEL_Absolute);
}