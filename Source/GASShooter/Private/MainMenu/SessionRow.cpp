// Copyright 2020 Dan Kestranek.

#include "MainMenu/SessionRow.h"
#include "MainMenu/MainMenu.h"

#include <Components/Button.h>
#include <Components/TextBlock.h>

void USessionRow::Setup(UMainMenu* parent, uint32 index)
{
	Parent = parent;
	Index = index;

	RowButton->OnClicked.AddDynamic(this, &USessionRow::OnClicked);
}

void USessionRow::OnClicked()
{
	if (Parent == nullptr) return;

	UE_LOG(LogTemp, Warning, TEXT("[USessionRow::OnClicked] Index %i"), Index);

	Parent->SelectIndexSessionsList(Index);
}
