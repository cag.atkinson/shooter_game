// Copyright 2020 Dan Kestranek.


#include "MainMenu/MainMenu.h"

#include "../../Public/Player/GSPlayerController.h"
#include "MainMenu/SessionRow.h"

#include <UObject/ConstructorHelpers.h>

#include <Components/WidgetSwitcher.h>
#include <Components/Button.h>
#include <Components/TextBlock.h>

bool UMainMenu::Initialize()
{
	bool bIsSuccess = Super::Initialize();

	if (!bIsSuccess) return false;

	if (NewSessionButton == nullptr) return false;
	NewSessionButton->OnClicked.AddDynamic(this, &UMainMenu::OnNewSessionPressed);

	if (JoinSessionButton == nullptr) return false;
	JoinSessionButton->OnClicked.AddDynamic(this, &UMainMenu::OnJoinSessionPressed);

	if (CancelJoinSessionButton == nullptr) return false;
	CancelJoinSessionButton->OnClicked.AddDynamic(this, &UMainMenu::OnCancelJoinSession);

	if (JoinSelectedSessionButton == nullptr) return false;
	JoinSelectedSessionButton->OnClicked.AddDynamic(this, &UMainMenu::OnJoinSelectedSession);

	return true;
}

void UMainMenu::Setup(ISessionMenuInterface* MenuInterface)
{
	SessionMenuInterface = MenuInterface;

	this->AddToViewport();

	UWorld* theWorld = GetWorld();
	if (theWorld == nullptr) return;

	AGSPlayerController* playerController = Cast<AGSPlayerController>(theWorld->GetFirstPlayerController());
	if (playerController == nullptr) return;

	// This sets the input mode for the Player Controller as UI only
	FInputModeUIOnly InputModeData;
	InputModeData.SetWidgetToFocus(this->TakeWidget());
	InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);

	playerController->SetInputMode(InputModeData);
	playerController->bShowMouseCursor = true;
}

void UMainMenu::TearDown()
{
	UWorld* theWorld = GetWorld();
	if (theWorld == nullptr) return;

	AGSPlayerController* playerController = Cast<AGSPlayerController>(theWorld->GetFirstPlayerController());
	if (playerController == nullptr) return;

	FInputModeUIOnly InputModeData;
	playerController->SetInputMode(InputModeData);
	playerController->bShowMouseCursor = true;

	this->RemoveFromViewport();
}

void UMainMenu::OnNewSessionPressed()
{
	if (SessionMenuInterface == nullptr) return;
	SessionMenuInterface->Host("Co-opShooterGameServer");
}

void UMainMenu::OnJoinSessionPressed()
{
	if ((MenuSwitcher == nullptr) || (SessionListMenuWidget == nullptr)) return;

	MenuSwitcher->SetActiveWidget(SessionListMenuWidget);

	if (SessionMenuInterface == nullptr) return;

	SessionMenuInterface->OpenSessionListMenu();
}

void UMainMenu::OnCancelJoinSession()
{
	if ((MenuSwitcher == nullptr) || (HostSessionMenuWidget == nullptr)) return;

	MenuSwitcher->SetActiveWidget(HostSessionMenuWidget);
}

void UMainMenu::OnJoinSelectedSession()
{
	if ((ScrollSessionList == nullptr) && (SessionMenuInterface == nullptr)) return;

	if (SelectedScrollIndex.IsSet())
	{
		int32 scrollCount = ScrollSessionList->GetChildrenCount();
		int32 selectedIndex = (int32)SelectedScrollIndex.GetValue();

		if ((scrollCount > 0) && (selectedIndex >= 0))
		{
			SessionMenuInterface->JoinSession(SelectedScrollIndex.GetValue());
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("[UMainMenu::OnJoinSelectedSession] No Sessions are available!"));
		}
	}
}

void UMainMenu::InitializeSessionsList(TArray<FServerData> Data)
{
	UE_LOG(LogTemp, Warning, TEXT("[UMainMenu::InitializeSessionsList] %i"), Data.Num());
	if (ScrollSessionList == nullptr) return;

	UWorld* theWorld = this->GetWorld();
	if (theWorld == nullptr) return;

	ScrollSessionList->ClearChildren();
	uint32 indexRow = 0;
	for (const FServerData& ServerData : Data)
	{
		UE_LOG(LogTemp, Warning, TEXT("[UMainMenu::InitializeSessionsList] %s"), *ServerData.Name);

		USessionRow* row = CreateWidget<USessionRow>(theWorld, SessionRowClass);
		if (row == nullptr) return;

		row->ServerName->SetText(FText::FromString(ServerData.Name));
		row->HostUser->SetText(FText::FromString(ServerData.HostUserName));

		FString FractionText = FString::Printf(TEXT("%d/%d"), ServerData.CurrentPlayers, ServerData.MaxPlayers);
		row->ConnectionFraction->SetText(FText::FromString(FractionText));

		row->Setup(this, indexRow);
		++indexRow;
		ScrollSessionList->AddChild(row);
	}

}

void UMainMenu::SelectIndexSessionsList(uint32 Index)
{
	UE_LOG(LogTemp, Warning, TEXT("[UMainMenu::InitializeSessionsList] Selected index: %i"), Index);

	SelectedScrollIndex = Index;
	UpdateSessionsList();
}

void UMainMenu::UpdateSessionsList()
{
	if (ScrollSessionList == nullptr) return;

	int rowIndex = 0;
	for (int32 i = 0; i < ScrollSessionList->GetChildrenCount() - 1; i++)
	{
		USessionRow* row = Cast<USessionRow>(ScrollSessionList->GetChildAt(i));
		if (row != nullptr)
		{
			row->Selected = (SelectedScrollIndex.IsSet() && (SelectedScrollIndex.GetValue() == rowIndex));
			rowIndex++;
		}
	}
}
